# lu4r\_ros\_node

The `lu4r_ros` is an interface to LU4R, providing the latter with transcribed sentences and retrieving interpretations, through HTTP communications. When launching the node, it requires the IP address and port of a running instance; then, the node waits until is ready to serve.

This node is subscribed to `/speech_hypotheses`, which contains the hypotheses list, encoded as a JSON string. This string is then sent to LU4R, whose reply is published onto the `/lu4r_response`, as interpretation encoded in format CFR.


## Dependencies

The package depends on the following resources:

 * `rospy` [http://wiki.ros.org/rospy]
 
## Communication

The node communicates with ROS through publishers/subscribers over the following topics.

### Subscribers

 * `/speech_hypotheses`: all the speech hypotheses produced by the Android app ASR that have to be sent to LU4R.
 
### Publisher
 
 * `/lu4r_response`: onto this topic the response from LU4R is published.

## Running

### Params

 * `lu4r_ip`: the IP address of a running LU4R server.
 * `lu4r_port`: the listening port of a running LU4R server.

### Startup

```
$ cd [YOUR_PATH]/lu4r_ros
$ roscore
$ rosrun lu4r_ros lu4r_ros_node.py _lu4r_ip:=127.0.0.1 _lu4r_port:=9000
```
