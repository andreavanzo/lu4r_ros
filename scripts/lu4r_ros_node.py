#!/usr/bin/env python

__author__ = "Andrea Vanzo"

#  Imports  #
# ROS imports
import rospy

# ROS msg and srv imports
from std_msgs.msg import String


# Python libraries
from lu4r_client import LU4RClient


#  Classes  #
class LU4RRosNode(object):

    def __init__(self):
        # Initialize node #
        rospy.init_node('lu4r_ros_node')
        
        # Reading params #
        self.lu4r_ip = rospy.get_param('~lu4r_ip', '127.0.0.1')
        self.lu4r_port = rospy.get_param('~lu4r_port', '5000')

        # Initialize publishers #
        self.lu4r_publisher = rospy.Publisher('/lu4r_response', String, queue_size=1)

        # Declare subscribers #
        rospy.Subscriber('/speech_hypotheses', String, self.speech_callback, queue_size=1)

        # Initialize LU4R #
        self.lu4r_client = LU4RClient(str(self.lu4r_ip), str(self.lu4r_port))

        print 'Waiting to serve you, Master!\n'
        rospy.spin()

    def speech_callback(self, data):
        print 'Master said:\t' + data.data
        interpretation = self.lu4r_client.parse_json(data.data)
        print 'LU4R says:\t\t' + interpretation + '\n'
        self.lu4r_publisher.publish(interpretation)


#  If Main  #
if __name__ == '__main__':
    LU4RRosNode()
